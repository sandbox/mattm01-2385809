<?php
/**
 * @file
 * Contains the callback fucntion that builds the admin form.
 */

/**
 * Page callback.
 */
function file_entity_link_display_mode_admin_settings_form($form, $form_state) {
  $form = array(
    'file_entity_link_display_mode_show_icon' => array(
      '#type' => 'checkbox',
      '#title' => t('Show File Icon?'),
      '#default_value' => variable_get('file_entity_link_display_mode_show_icon', 0),
      '#description' => t("Check this box if you want to display the file icon before the file entity link."),
    ),
  );

  return system_settings_form($form);
}
