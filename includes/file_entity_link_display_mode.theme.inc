<?php
/**
 * @file
 * Contains the Drupal Hook that themes file links.
 */

/**
 * Implements theme_file_link().
 */
function theme_file_entity_file_entity_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];
  $show_icon = variable_get('file_entity_link_display_mode_show_icon', 0);
  $icon = '';

  if($show_icon):
    $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));
    $icon .= ' ';
  endif;

  return "<span class='file'><a href='/file/{$file->fid}'>{$file->filename}</a> {$icon}</span>";
}
